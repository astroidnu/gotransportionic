// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('goTransport', ['ionic'])

.run(function($ionicPlatform,$ionicPopup) {
  $ionicPlatform.ready(function() {
    //checking internet status
        if(window.Connection) {
                if(navigator.connection.type == Connection.NONE) {
                    $ionicPopup.confirm({
                        title: "Internet Disconnected",
                        content: "No Connection Found !, Please Active you Internet Access !"
                    })
                    .then(function(result) {
                        if(!result) {
                            ionic.Platform.exitApp();
                        }
                    });
                }
            }
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
   $ionicPlatform.registerBackButtonAction(function (event) {
            //ionic.Platform.exitApp();
             $ionicPopup.confirm({
                title: "Confirmation Exit App",
                content: "Are you sure for exit the app?"
              })
              .then(function(result) {
              if(result) {
                ionic.Platform.exitApp();
                }
              });
                }, 100);
})
//INISIALISASI HALAMAN YANG ADA PADA GO TRANSPORT
.config(function($stateProvider, $urlRouterProvider){
    //PENDAFTARAN HALAMAN PADA FUNGSI CONFIG DI JS
    $stateProvider
    .state('pilihAngkot',{
      controller:'pilihAngkotCtrl',
      templateUrl:'templates/pilihAngkot.html',
      url:'/pilihAngkot'
    })
    .state('pilihRute',{
      controller:'pilihRuteCtrl',
      cache:'false',
      templateUrl:'templates/pilihRute.html',
      url:'/pilihRute'
    })
    .state('listAngkot',{
       cache:'false',
      controller:'listAngkotCtrl',
      templateUrl:'templates/listAngkot.html',
      url:'/listAngkot'
    })
    .state('petaPerjalanan',{
       cache:'false',
      controller:'petaCtrl',
      templateUrl:'templates/peta.html',
      url:'/petaPerjalanan'
    })
    .state('tutorial',{
      controller:'tutorialCtrl',
      templateUrl:'templates/tutorial.html',
      url:'/tutorial'
    })
    .state('tentang',{
       cache:'false',
      controller:'tentangCtrl',
      templateUrl:'templates/tentang.html',
      url:'/tentang'
    })
    //SET DEFAULT PAGE
    $urlRouterProvider.otherwise('/tutorial');
})
.controller('tentangCtrl',function($scope,$state,dataAngkot,dataKeberangkatan,dataTujuan,$ionicHistory){
   $scope.back = function(){
    $ionicHistory.clearCache();
     $state.go('pilihAngkot');
   }
})
.controller('tutorialCtrl',function($scope,$ionicSlideBoxDelegate,$state){
   $scope.navSlide = function() {
       var index = $ionicSlideBoxDelegate.currentIndex();
    }

    $scope.pindah = function(){
      $state.go('pilihAngkot');
    }

})
//CONTROLLER UNTUK PILIH ANGKOT
.controller('pilihAngkotCtrl',function($scope,$ionicPlatform,$ionicPopup,$state,dataAngkot){

  $scope.exit = function(){
    var confirmPopUp = $ionicPopup.confirm({
     title: 'Perhatian !',
     template: 'Apakah Anda yakin untuk keluar ?'
   });
   confirmPopUp.then(function(res) {
     if(res) {
       ionic.Platform.exitApp();
     } else {
       $state.go('pilihAngkot') 
     }
   });
  }
  $scope.jenisAngkot = function(newObj){
      dataAngkot.addAngkot(newObj);
      $state.go('pilihRute')
  }
})
//PASSING DATA BETWEEN CONTROLLER
.service('dataAngkot',function(){
  var AngkotList = [];

  var addAngkot = function(newObj) {
      AngkotList.push(newObj);
  }

  var getAngkot = function(){
      return AngkotList;
  }

  var deleteAngkotList = function(){
      AngkotList = [];  
  }

  return {
    addAngkot: addAngkot,
    deleteAngkotList:deleteAngkotList,
    getAngkot: getAngkot
  };
})
//CONTROLLER UNTUK MAP
.controller('petaCtrl',function($ionicHistory,$scope,$ionicLoading, $compile,$http,dataKeberangkatan,dataTujuan,$state,dataAngkot){
//deklarasi variabel untuk map
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
$scope.initialize = function() {
    calcRoute();
  directionsDisplay = new google.maps.DirectionsRenderer();
  var jakarta = new google.maps.LatLng(-6.208763, 106.845599);
  var mapOptions = {
    zoom:10,
    center: jakarta
  };
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  directionsDisplay.setMap(map);
    $scope.map = map;
}
//pembuatan rute 
function calcRoute() {
    //deklarasi variabel untuk getting latlang
      var getDTKeberangkatan = dataKeberangkatan.getKeberangkatan() + ",Jakarta Indonesia";
      var getDTTujuan = dataTujuan.getTujuan() + ",Jakarta Indonesia";
       var getDTKeberangkatan2 = dataKeberangkatan.getKeberangkatan() ;
      var getDTTujuan2 = dataTujuan.getTujuan() ;
      var getDTAngkot = dataAngkot.getAngkot(); 
      var waypts = [];
  //get data ulang 
  $http.get("http://personalict.zz.mu/data_trayek/?posisi="+getDTKeberangkatan2+"&tujuan="+getDTTujuan2+"&jenis_angkutan="+getDTAngkot)
    .success(function(data) {
        //FUNGSI HIDE LOADING
       
      var data2 = data.data_trayek_bus[0].rute_map;
      console.log(data2);
         console.log(data2.length);
      for (var i =0; i <= data2.length-1; i++) {
            waypts.push({
          location:data2[i]+", Provinsi DKI Jakarta Indonesia",
          stopover:true});
          
      };
  //getting latlang form json 
$http.get("https://maps.googleapis.com/maps/api/directions/json?origin="+getDTKeberangkatan+"&destination="+getDTTujuan+"&key=AIzaSyANrsrdlwdwpf26LcfeOcpXVISScjPc7L4")
.success(function(data){
  console.log(data); 
    console.log(data);
   var berangkatLat = data.routes[0].legs[0].start_location.lat;
   var berangkatLng = data.routes[0].legs[0].start_location.lng;
   var tujuanLat = data.routes[0].legs[0].end_location.lat;
   var tujuanLng = data.routes[0].legs[0].end_location.lng;

  var start = berangkatLat+','+berangkatLng;
  var end = tujuanLat+','+tujuanLng;
  var a = waypts[2];
  var b = waypts[3];
  var c = waypts[5];
  var d = waypts[6];
  var request = {
      origin:start,
      destination:end,
      waypoints:[a,b,c,d],
      optimizeWaypoints: true,
       travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
     console.log(dataAngkot.getAngkot());
    console.log(dataKeberangkatan.getKeberangkatan())
    console.log(dataTujuan.getTujuan());
});
});
}
 //back state function
  $scope.back = function(){
    dataAngkot.deleteAngkotList();
    dataKeberangkatan.deleteKeberangkatan();
    dataTujuan.deleteTujuan();
    $ionicHistory.clearCache();
    $state.go('pilihAngkot');
 }
})
//CONTROLLER UNTUK PILIH RUTE
.controller('pilihRuteCtrl',function($scope,$state,dataAngkot,dataKeberangkatan,dataTujuan,$ionicPopup,$ionicHistory){
  //FUNGSI UNTUK ALERT 
  var alertData = function(){
       var alertPopup = $ionicPopup.alert({
           title: 'Upss kesalahan data rute !!',
           template: 'Periksa kembali rute perjalanan Anda '
         });
         alertPopup.then(function(res) {
             $state.go('pilihRute');
         });
  }
  $scope.back = function(){
       $state.go('pilihAngkot');
       $ionicHistory.clearCache();
      dataAngkot.deleteAngkotList();
  }
  //BUAT ARRAY UNTUK MODEL 
  $scope.data = {
    keberangkatan : '',
    tujuan : ''
  };
  $scope.submit = function(){
    //GETTING LENGTH UNTUK VERIFIKASI DATA INPUTAN
    var lengthKeberangkatan = $scope.data.keberangkatan.length;
    var lengthTujuan = $scope.data.tujuan.length;
    //SIMPAN NILAI DARI INPUTAN KE DALAM VARIABLE
    var dtBerangkat = $scope.data.keberangkatan;
    var dtTujuan = $scope.data.tujuan;
    var dtBerangkatUpp = dtBerangkat.toUpperCase();
    var dtTujuanUpp = dtTujuan.toUpperCase();
    //KONDISI DIMANA INPUTAN HARUS MEMENUHI SYARAT -> NILAI SETIAP VARIABEL HARUS DIPERIKSA
    //MINIMAL INPUTAN = 4 AGAR TIDAK TERJADI KESALAHAN
    //DAN APABILA NILAI KEBERANGKATAN  = NILAI TUJUAN MAKA TIDAK VALID ATAU SEBALIKNYA
    if (lengthKeberangkatan<4 || dtBerangkatUpp === dtTujuanUpp) {
        alertData();
    }else if (lengthTujuan<4 || dtBerangkatUpp === dtTujuanUpp ) {
        alertData();
    }
    else if((lengthKeberangkatan<4) && (lengthTujuan <4)){
        alertData();
    }else {
      console.log($scope.data.keberangkatan.length);
      //ADDING NEW VALUE INPUT KE DALAM MASING2 SERVICE
      dataKeberangkatan.addKeberangkatan(dtBerangkat);
      dataTujuan.addTujuan(dtTujuan);   
      $state.go('listAngkot');
    }
  }  
})
//SERVICE UNTUK DATA KEBERANGKATAN
.service('dataKeberangkatan', function(){
  var dataKeberangkatan = [];

  var addKeberangkatan = function(newObj){
       dataKeberangkatan.push(newObj);
  }

  var getKeberangkatan = function(){
    return dataKeberangkatan;
  }

  var deleteKeberangkatan = function(){
     dataKeberangkatan = [];
  }
  return{
    addKeberangkatan: addKeberangkatan,
    getKeberangkatan:getKeberangkatan,
    deleteKeberangkatan : deleteKeberangkatan
  }
})
//cache untuk maps
.factory('myCache', function($cacheFactory) {
 return $cacheFactory('myData');
})
//SERVICE UNTUK DATA TUJUAN
.service('dataTujuan',function(){
  //MEMBUAT SEBUAH LIST UNTUK MENAMPUNG DATA 
  var dataTujuan = [];

  var addTujuan = function(newObj){
     dataTujuan.push(newObj);
  }
  var getTujuan = function(){
    return dataTujuan;
  }
  var deleteTujuan = function(){
    dataTujuan = [];
  }

  return {
    addTujuan : addTujuan,
    getTujuan : getTujuan,
    deleteTujuan : deleteTujuan,
  }
})
//CONTROLLER UNTUK LIST ANGKOT PAGE
.controller('listAngkotCtrl',function($ionicHistory,$scope,$state,dataAngkot,dataKeberangkatan,dataTujuan,$http,$ionicLoading,$ionicPopup,$ionicActionSheet, $timeout,myCache){
      $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
      //GETTING NEW DATA FROM MASING2 SERVICE AND SAVE IN EACH VARIABLE
      var getDTAngkot = dataAngkot.getAngkot();
      var getDTKeberangkatan = dataKeberangkatan.getKeberangkatan();
      var getDTTujuan = dataTujuan.getTujuan();
      //BOOL VALUE
      respData = false;
      //LOADING 
      $ionicLoading.show({
          template: '<i class="spinner-a">Loading..</i>',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 400,
          showDelay: 0
      });
      //SET DEFAULT TIME OUT JDI KALAU LEMOT BISA KE HANDLE 
      $timeout(function(){$ionicLoading.hide();},2000);
      //GET DATA FROM JSON HTTP DATA ID
     $http.get("http://personalict.zz.mu/data_trayek/?posisi="+getDTKeberangkatan+"&tujuan="+getDTTujuan+"&jenis_angkutan="+getDTAngkot)
    .success(function(data) {
        //FUNGSI HIDE LOADING
        var hideLoading = function(){
           $ionicLoading.hide();
        }
        $scope.data = data.data_trayek_bus;
        console.log(data);
        var dtArr = $scope.data.length;
        if(dtArr<1){
           hideLoading();
           var alertPopup = $ionicPopup.alert({
           title: 'Upss.. data angkutan tidak ditemukan !'
         });
         alertPopup.then(function(res) {
            dataKeberangkatan.deleteKeberangkatan();
            dataTujuan.deleteTujuan();
             $state.go('pilihRute');
         });
        }else{
             hideLoading();
        }   
    })
    .error(function(data){
      
    });
      $scope.back = function(){
        $ionicHistory.clearCache();
          dataKeberangkatan.deleteKeberangkatan();
          dataTujuan.deleteTujuan();
          $state.go('pilihRute');
      }

      $scope.menuOpsi = function(){
           // Show the action sheet
   var hideSheet = $ionicActionSheet.show({
     buttons: [
       { text: 'Peta Perjalanan' },
       { text: 'Tentang'}
     ],
     buttonClicked: function(index) {
           if(index===0){
      $state.transitionTo('petaPerjalanan'); 
        }
           else if(index===1){
      $state.transitionTo('tentang'); 
        }
     }
   });

   // For example's sake, hide the sheet after two seconds
   $timeout(function() {
     hideSheet();
   }, 2000);
      };
});